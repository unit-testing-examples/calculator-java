package lib;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculatorTests {
    
    @Test
    public void given_two_numbers_when_add_then_it_should_calculate_the_sum_of_them() {
        Calculator calculator = new Calculator();
        
        int res = calculator.add(1, 2);
        
        assertEquals(3, res);
    }
    
    @Test
    public void given_two_numbers_when_substract_then_it_should_calculate_the_difference_between_them() {
        Calculator calculator = new Calculator();
        
        int res = calculator.substract(3, 7);
        
        assertEquals(-4, res);
    }
}